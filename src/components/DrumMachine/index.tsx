// External dependencies
import {
	createElement as h,
	createRef,
	KeyboardEvent,
	useMemo,
	useState,
} from 'react'

// Project component dependencies
import DrumPad from '../DrumPad'

// Internal dependencies
import './style.css'

type DrumPadData = {
	id: string
	name: string
	keyCommand: string
	src: string
}

type Props = {
	drumPads: Array<DrumPadData>
}

export default function DrumMachine({ drumPads }: Props): JSX.Element {
	const [lastPadUsed, setLastPadUsed] = useState(
		'Click a pad or press a key and make a beat!'
	)

	// Refs to buttons in child DrumPad components.
	const buttonRefs = useMemo(
		() => drumPads.map(() => createRef<HTMLButtonElement>()),
		[drumPads.length]
	)

	function handleKeyDown({ key }: KeyboardEvent): void {
		// Find the drum pad data object that matches the key pressed, if it exists.
		const matchingDrumPad = drumPads.find(
			drumPad => drumPad.keyCommand === key.toUpperCase()
		)

		// If the drum pad exists, click the associated button.
		if (matchingDrumPad !== undefined) {
			const matchingButton =
				buttonRefs[drumPads.indexOf(matchingDrumPad)].current
			matchingButton?.click()
		}
	}

	return (
		// eslint-disable-next-line jsx-a11y/no-static-element-interactions
		<div id="drum-machine" className="drum-machine" onKeyDown={handleKeyDown}>
			<div id="display">{lastPadUsed}</div>
			<div className="drum-machine__drum-pads">
				{drumPads.map(({ id, name, keyCommand, src }, index) => (
					<DrumPad
						key={id}
						ref={buttonRefs[index]}
						id={id}
						name={name}
						keyCommand={keyCommand}
						src={src}
						setLastPadUsed={setLastPadUsed}
						// eslint-disable-next-line jsx-a11y/no-autofocus
						autoFocus={index === 0 ? true : undefined}
					/>
				))}
			</div>
		</div>
	)
}
