// External dependencies
import {
	createElement as h,
	forwardRef,
	useRef,
	Dispatch,
	RefObject,
	SetStateAction,
} from 'react'

// Internal dependencies
import './style.css'

function playAudioEl(audioRef: RefObject<HTMLAudioElement>): void {
	const audioEl = audioRef.current
	if (audioEl !== null) {
		audioEl.pause()
		audioEl.currentTime = 0
		void audioEl.play()
	}
}

type Props = {
	autoFocus?: boolean
	id: string
	keyCommand: string
	name: string
	setLastPadUsed: Dispatch<SetStateAction<string>>
	src: string
}

// https://reactjs.org/docs/forwarding-refs.html#forwarding-refs-to-dom-components
export default forwardRef<HTMLButtonElement, Props>(
	(
		{ autoFocus, id, name, keyCommand, src, setLastPadUsed },
		ref
	): JSX.Element => {
		const audioRef = useRef<HTMLAudioElement>(null)

		return (
			<button
				ref={ref}
				id={id}
				className="drum-pad"
				// eslint-disable-next-line jsx-a11y/no-autofocus
				autoFocus={autoFocus}
				onClick={() => {
					playAudioEl(audioRef)
					setLastPadUsed(name)
				}}
			>
				{keyCommand}
				<audio ref={audioRef} id={keyCommand} className="clip" src={src} />
			</button>
		)
	}
)
