// External dependencies
import { createElement as h } from 'react'

// Project component dependencies
import DrumMachine from '../DrumMachine'
import Footer from '../Footer'

// Internal dependencies
import './style.css'

/*
Sounds from:
https://freesound.org/people/matiasromero/sounds/78819/
https://freesound.org/people/Walter_Odington/sounds/25602/
https://freesound.org/people/flameout/sounds/214438/
https://freesound.org/people/deleted_user_2195044/sounds/267731/
https://freesound.org/people/shnur_/sounds/336962/
https://freesound.org/people/menegass/sounds/99751/
https://freesound.org/people/young_daddy/sounds/24786/
https://freesound.org/people/tmog.762/sounds/385944/
https://freesound.org/people/zinzan_101/sounds/91789/

All these sounds are in the public domain via the Creative Commons Zero legal tool:
https://creativecommons.org/share-your-work/public-domain/cc0
*/
import snare1 from './sounds/snare1.oga'
import bassDrum from './sounds/bass-drum.opus'
import snare2 from './sounds/snare-2.oga'
import cookingUtensil from './sounds/cooking-utensil.oga'
import tinnyDrum from './sounds/tinny-drum.oga'
import openBongo from './sounds/open-bongo.oga'
import clap from './sounds/clap.oga'
import synthKick1 from './sounds/synth-kick-1.oga'
import synthKick2 from './sounds/synth-kick-2.oga'

const myDrumPads = [
	{
		id: 'snare-1',
		name: 'snare 1',
		keyCommand: 'Q',
		src: snare1,
	},
	{
		id: 'bass-drum',
		name: 'bass drum',
		keyCommand: 'W',
		src: bassDrum,
	},
	{
		id: 'snare-2',
		name: 'snare 2',
		keyCommand: 'E',
		src: snare2,
	},
	{
		id: 'cooking-utensil',
		name: 'cooking utensil',
		keyCommand: 'A',
		src: cookingUtensil,
	},
	{
		id: 'tinny-drum',
		name: 'tinny drum',
		keyCommand: 'S',
		src: tinnyDrum,
	},
	{
		id: 'open-bongo',
		name: 'open bongo',
		keyCommand: 'D',
		src: openBongo,
	},
	{
		id: 'clap',
		name: 'clap',
		keyCommand: 'Z',
		src: clap,
	},
	{
		id: 'synth-kick-1',
		name: 'synth kick 1',
		keyCommand: 'X',
		src: synthKick1,
	},
	{
		id: 'synth-kick-2',
		name: 'synth kick 2',
		keyCommand: 'C',
		src: synthKick2,
	},
]

export default function App(): JSX.Element {
	return (
		<div className="zeb-drum-machine-app">
			<DrumMachine drumPads={myDrumPads} />
			<Footer />
		</div>
	)
}
