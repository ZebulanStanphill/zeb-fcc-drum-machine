declare module '*.oga' {
	const content: string
	export default content
}

declare module '*.opus' {
	const content: string
	export default content
}
