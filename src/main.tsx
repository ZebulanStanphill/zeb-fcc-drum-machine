// External dependencies
import { createElement as h } from 'react'
import { render } from 'react-dom'

// Internal dependencies
import './style.css'
import App from './components/App'

render(<App />, document.body)
