/* eslint-env node */
const path = require('path')
const HtmlPlugin = require('html-webpack-plugin')
const MiniCssExtractPlugin = require('mini-css-extract-plugin')

module.exports = {
	mode: 'production',
	context: path.resolve(__dirname, 'src'),
	entry: './main.tsx',
	output: {
		path: path.resolve(__dirname, 'dist'),
		filename: 'bundle.[hash].js',
		clean: true,
	},
	devtool: 'source-map',
	module: {
		rules: [
			{
				test: /\.jsx?$/u,
				use: ['babel-loader'],
			},
			{
				test: /\.tsx?$/u,
				use: ['babel-loader', 'ts-loader'],
			},
			{
				test: /\.css$/u,
				use: [MiniCssExtractPlugin.loader, 'css-loader'],
			},
			{
				test: /\.(oga|opus)$/u,
				type: 'asset/resource',
				generator: {
					filename: 'sounds/[name][ext][query]',
				},
			},
			{
				test: /\.(ttf)$/u,
				type: 'asset/resource',
				generator: {
					filename: 'fonts/[name][ext][query]',
				},
			},
		],
	},
	resolve: {
		/* Without this, importing .tsx or .ts files without specifying the extension won't work, and TypeScript won't let you use explicit extensions in imports. */
		extensions: ['.tsx', '.ts', '.jsx', '.js'],
	},
	plugins: [
		new HtmlPlugin({
			template: 'index.ejs',
			meta: {
				viewport: 'width=device-width, initial-scale=1',
			},
			title: 'Zeb’s freeCodeCamp Drum Machine',
			xhtml: true,
		}),
		new MiniCssExtractPlugin({
			filename: 'style.[hash].css',
		}),
	],
}
